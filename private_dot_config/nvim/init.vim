set number
set list

set listchars=tab:▸\ ,eol:¬

set tabstop=4
set softtabstop=4
set shiftwidth=4
set noexpandtab

packadd minpac
call minpac#init()
call minpac#add('k-takata/minpac', {'type': 'opt'})

call minpac#add('tpope/vim-unimpaired')
call minpac#add('tpope/vim-projectionist')
call minpac#add('sgur/vim-editorconfig')
call minpac#add('tpope/vim-obsession')
call minpac#add('rakr/vim-one')
call minpac#add('dracula/vim', {'name': 'dracula'})
call minpac#add('airblade/vim-gitgutter')
call minpac#add('tpope/vim-commentary')
call minpac#add('neovim/nvim-lspconfig', {'type': 'opt'})
packadd nvim-lspconfig
call minpac#add('hrsh7th/cmp-nvim-lsp')
call minpac#add('hrsh7th/cmp-buffer')
call minpac#add('hrsh7th/nvim-cmp')
call minpac#add('onsails/lspkind-nvim')

" nvim-cmp requires a snippet engine.
call minpac#add('hrsh7th/cmp-vsnip')
call minpac#add('hrsh7th/vim-vsnip')

" Display method signatures while typing
call minpac#add('ray-x/lsp_signature.nvim')

call minpac#add('nvim-treesitter/nvim-treesitter')

call minpac#add('nvim-lua/plenary.nvim')
call minpac#add('nvim-telescope/telescope.nvim')
call minpac#add('mrjones2014/dash.nvim', {'do': 'make install'})

call minpac#add('kyazdani42/nvim-web-devicons')

call minpac#add('jose-elias-alvarez/null-ls.nvim')
call minpac#add('jose-elias-alvarez/nvim-lsp-ts-utils')

call minpac#add('folke/trouble.nvim')
call minpac#add('ggandor/leap.nvim')

" Completion settings
set completeopt=menu,menuone,noselect

lua <<EOF
	local cmp = require'cmp'
	local lspkind = require('lspkind')

	cmp.setup({
		snippet = {
			expand = function(args)
				vim.fn['vsnip#anonymous'](args.body)
			end,
		},
		mapping = {
			['<C-n>'] = cmp.mapping.select_next_item(),
			['<C-p>'] = cmp.mapping.select_prev_item(),
			['<C-d>'] = cmp.mapping.scroll_docs(-4),
			['<C-f>'] = cmp.mapping.scroll_docs(4),
			['<C-Space>'] = cmp.mapping.complete(),
			['<C-e>'] = cmp.mapping.close(),
		},
		sources = {
			{ name = 'nvim_lsp' },
			{ name = 'vsnip' },
			{ name = 'buffer' },
		},
		formatting = {
			format = lspkind.cmp_format({with_text = true, menu = ({
				buffer = "[Buffer]",
				nvim_lsp = "[LSP]",
				nvim_lua = "[Lua]"
			})})
		},
	})

	local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())

	require('lspconfig')['ts_ls'].setup {
		capabilities = capabilities,
		on_attach = function(client, bufnr)
			client.server_capabilities.document_formatting = false
			client.server_capabilities.document_range_formatting = false

			local ts_utils = require('nvim-lsp-ts-utils')
			ts_utils.setup({
				eslint_bin = 'eslint_d',
				eslint_enable_diagnostics = true,
				eslint_enable_code_actions = true,
				enable_formatting = true,
				formatted = 'prettier'
			})
			ts_utils.setup_client(client)

			--on_attach(client, bufnr)
		end
	}
	--require('lspconfig')['solargraph'].setup {
		--capabilities = capabilities
	--}
	require('lspconfig')['rls'].setup {
		capabilities = capabilities
	}
	require('lspconfig')['sorbet'].setup {
		capabilities = capabilities
	}

	require('null-ls').setup({
		sources = {
			require('null-ls').builtins.diagnostics.eslint_d,
			require('null-ls').builtins.formatting.prettier.with({
				prefer_local = 'node_modules/.bin',
			}),
		},
	})

	require('trouble').setup {
--		auto_open = true,
--		auto_close = true,
	}

	-- require('leap').set_default_keymaps()
EOF

" lsp_signature
lua <<EOF
	require('lsp_signature').setup()
EOF

" TreeSitter
lua << EOF
require'nvim-treesitter.configs'.setup {
	highlight = { enable = true },
	indent = { enable = true }
}
EOF

" Devicons
lua << EOF
require'nvim-web-devicons'.setup {
	default = true;
}
EOF

" :PackUpdate to update plugins, and :PackClean to clean out removed ones.
command! PackUpdate call minpac#update()
command! PackClean call minpac#clean()

" Telescope
nnoremap <C-p> <cmd>Telescope git_files<cr>
nnoremap <C-o> <cmd>Telescope buffers<cr>

set termguicolors
silent! colorscheme dracula
set background=dark
let g:one_allow_italics = 1
highlight Comment cterm=italic

set mouse=a

" LSP Commands
nnoremap <silent> K <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> gD <cmd>lua vim.diagnostic.open_float()<CR>

" Trouble
nnoremap <silent> gX <cmd>TroubleToggle<CR>

" Leap
nnoremap <silent> t <plug>(leap-forward)
nnoremap <silent> T <plug>(leap-backward)

" GitGutter
set updatetime=100
